# EEStest.test

## Basic information

- Dockerized application (PHP 8.2, MariaDB, Redis) (I've been wanting to use Redis for caching but haven't gotten around to it yet)
- Can seed the database with fake data (used by the factory)
- The application contains tests ([Pest](https://pestphp.com/)) to cover basic scenarios
- Validation requests could be unified (because Store and Update validation is the same) - StoreMemberRequest.php & UpdateMemberRequest.php
- [Laravel Pint](https://laravel.com/docs/10.x/pint) was used for uniform code formatting
- The application contains actions so that each action stands for what it is created for. It would be useful for future expansion of the application. The Repository (MemberRepository) can thus be easily expanded with new actions, and the logic of the individual steps will be in the actions.
- To create the API documentation, I first wanted to use [Scramble](https://scramble.dedoc.co/) or [Scribe](https://scribe.knuckles.wtf/laravel/), but then I read in the requirements that it should be created in OpenAPI or in Markdown. Unfortunately, I read OpenAI instead of OpenAPI, first I went through OpenAI - I just edited it a little and added it to the Readme.md file - i.e. in markdown format.
- Although part of the task was to avoid using external packages and libraries, I wanted to "sell myself" more - so I used Pint and Pest in addition :) I didn't need to use Pest and could have created tests without this package, but Pest made creating sample tests faster.

## Tests

These items are tested:

- Actions
- Controllers
- Repositories

## API Docs

In the root folder of the project, you can download the collection to Postman.

### members.index [GET]

Example:

```
http://laravel.eostest.test:8989/api
/v1/members
```

### members.store [POST]

Example:

```
http://laravel.eostest.test:8989/api
/v1/members
```

- name (string) - required|string|max:255
- surname (string) - required|string|max:255
- email (string) - required|email|max:255|unique:members,email
- birthdate (string) - required|date

### members.show [GET]

Example:

```
http://laravel.eostest.test:8989/api
/v1/members
```

### members.update [PUT]

Example:

```
http://laravel.eostest.test:8989/api
/v1/members/{member}
```

- name (string) - required|string|max:255
- surname (string) - required|string|max:255
- email (string) - required|email|max:255|unique:members,email
- birthdate (string) - required|date

### members.destroy [DELETE]

Example:

```
http://laravel.eostest.test:8989/api
/v1/members/{member}
```

### members.tags [POST]

Example:

```
http://laravel.eostest.test:8989/api
/v1/members/{member}/tags
```

- member_tag_id (integer) - required|exists:member_tags,id
