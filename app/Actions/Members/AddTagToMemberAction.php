<?php

namespace App\Actions\Members;

use App\Models\Member;

class AddTagToMemberAction
{
    public function execute(Member $member, array $attributes): Member
    {
        $member->memberTags()->syncWithoutDetaching($attributes['member_tag_id']);

        return $member;
    }
}
