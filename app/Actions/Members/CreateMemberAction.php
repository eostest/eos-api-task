<?php

namespace App\Actions\Members;

use App\Models\Member;

class CreateMemberAction
{
    public function execute(array $attributes): Member
    {
        return Member::create($attributes);
    }
}
