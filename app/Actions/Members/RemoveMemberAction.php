<?php

namespace App\Actions\Members;

use App\Models\Member;

class RemoveMemberAction
{
    public function execute(Member $member): void
    {
        $member->delete();
    }
}
