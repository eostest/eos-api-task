<?php

namespace App\Actions\Members;

use App\Models\Member;

class UpdateMemberAction
{
    public function execute(Member $member, array $attributes): Member
    {
        $member->update($attributes);

        return $member;
    }
}
