<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Members\AddMemberTagRequest;
use App\Http\Requests\Members\StoreMemberRequest;
use App\Http\Requests\Members\UpdateMemberRequest;
use App\Http\Resources\MemberResource;
use App\Models\Member;
use App\Repositories\Interfaces\MemberRepositoryInterface;

class MemberController extends Controller
{
    public function __construct(private MemberRepositoryInterface $repository)
    {
    }

    public function index()
    {
        return MemberResource::collection($this->repository->getPaginatedMembers(5));
    }

    public function store(StoreMemberRequest $request)
    {
        $member = $this->repository->createMember($request->validated());

        return new MemberResource($member);
    }

    public function show(Member $member)
    {
        return new MemberResource($member);
    }

    public function update(UpdateMemberRequest $request, Member $member)
    {
        $member = $this->repository->updateMember($member, $request->validated());

        return new MemberResource($member);
    }

    public function destroy(Member $member)
    {
        $this->repository->deleteMember($member);

        return response()->json(null, 204);
    }

    public function addTag(AddMemberTagRequest $request, Member $member)
    {
        $member = $this->repository->addTagToMember($member, $request->validated());

        return new MemberResource($member);
    }
}
