<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MemberResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $memberData = [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'email' => $this->email,
            'birthdate' => $this->birthdate->format('Y-m-d'),
        ];

        if (request()->tags && request()->tags == 1) {
            $memberData['member_tags'] = MemberTagResource::collection($this->memberTags);
        }

        return $memberData;
    }
}
