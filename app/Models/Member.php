<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Member extends Model
{
    use HasFactory;

    public $fillable = [
        'name',
        'surname',
        'email',
        'birthdate',
    ];

    protected $casts = [
        'birthdate' => 'date',
    ];

    public function memberTags(): BelongsToMany
    {
        return $this->belongsToMany(MemberTag::class);
    }
}
