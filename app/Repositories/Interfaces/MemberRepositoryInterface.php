<?php

namespace App\Repositories\Interfaces;

use App\Models\Member;

interface MemberRepositoryInterface
{
    public function getAllMembers();

    public function getPaginatedMembers(int $itemsPerPage);

    public function getMemberById(Member $member);

    public function createMember(array $attributes);

    public function updateMember(Member $member, array $attributes);

    public function deleteMember(Member $member);

    public function addTagToMember(Member $member, array $attributes);
}
