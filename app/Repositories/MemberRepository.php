<?php

namespace App\Repositories;

use App\Actions\Members\AddTagToMemberAction;
use App\Actions\Members\CreateMemberAction;
use App\Actions\Members\RemoveMemberAction;
use App\Actions\Members\UpdateMemberAction;
use App\Models\Member;
use App\Repositories\Interfaces\MemberRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class MemberRepository implements MemberRepositoryInterface
{
    public function getAllMembers(): Collection
    {
        return Member::all();
    }

    public function getPaginatedMembers(int $itemsPerPage): LengthAwarePaginator
    {
        return Member::paginate($itemsPerPage);
    }

    public function getMemberById(Member $member): Member
    {
        return $member;
    }

    public function createMember(array $attributes): Member
    {
        return (new CreateMemberAction())->execute($attributes);
    }

    public function updateMember(Member $member, array $attributes): Member
    {
        return (new UpdateMemberAction())->execute($member, $attributes);
    }

    public function deleteMember(Member $member): void
    {
        (new RemoveMemberAction())->execute($member);
    }

    public function addTagToMember(Member $member, array $attributes): Member
    {
        return (new AddTagToMemberAction())->execute($member, $attributes);
    }
}
