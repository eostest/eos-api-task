<?php

namespace Database\Seeders;

use App\Models\Member;
use App\Models\MemberTag;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Member::factory()
            ->has(MemberTag::factory()->count(5))
            ->count(10)
            ->create();
    }
}
