<?php

use App\Actions\Members\CreateMemberAction;
use Illuminate\Support\Carbon;

it('creates member', function () {
    $attributes = [
        'name' => 'Will',
        'surname' => 'Smith',
        'email' => 'will@smith.com',
        'birthdate' => '1968-09-25',
    ];

    $member = (new CreateMemberAction())->execute($attributes);

    expect($member)
        ->name
        ->not->toBeNull()
        ->toBeString()
        ->toEqual($attributes['name'])
        ->surname
        ->not->toBeNull()
        ->toBeString()
        ->toEqual($attributes['surname'])
        ->email
        ->not->toBeNull()
        ->toBeString()
        ->toEqual($attributes['email'])
        ->birthdate
        ->not->toBeNull()
        ->toBeInstanceOf(Carbon::class)
        ->toEqual(Carbon::parse($attributes['birthdate']));
});
