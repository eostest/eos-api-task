<?php

use App\Actions\Members\RemoveMemberAction;
use App\Models\Member;

it('removes member', function () {
    $member = Member::factory()->create();

    expect(Member::count())->toEqual(1);

    (new RemoveMemberAction())->execute($member);

    expect(Member::count())->toEqual(0);
});
