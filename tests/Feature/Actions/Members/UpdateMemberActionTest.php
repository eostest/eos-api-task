<?php

use App\Actions\Members\UpdateMemberAction;
use App\Models\Member;
use Illuminate\Support\Carbon;

it('updates member', function () {
    $member = Member::factory()->create();

    $attributes = [
        'name' => 'Will',
        'surname' => 'Smith',
        'email' => 'will@smith.com',
        'birthdate' => '1968-09-25',
    ];

    (new UpdateMemberAction())->execute($member, $attributes);

    expect($member)
        ->name
        ->not->toBeNull()
        ->toBeString()
        ->toEqual($attributes['name'])
        ->surname
        ->not->toBeNull()
        ->toBeString()
        ->toEqual($attributes['surname'])
        ->email
        ->not->toBeNull()
        ->toBeString()
        ->toEqual($attributes['email'])
        ->birthdate
        ->not->toBeNull()
        ->toBeInstanceOf(Carbon::class)
        ->toEqual(Carbon::parse($attributes['birthdate']));
});
