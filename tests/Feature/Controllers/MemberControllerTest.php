<?php

use App\Models\Member;
use App\Models\MemberTag;

it('returns a paginated list of members', function () {
    Member::factory()->count(10)->create();

    $response = $this->getJson(route('members.index'));

    $response->assertOk();
    $response->assertJsonStructure([
        'data' => [
            '*' => [
                'id',
                'name',
                'surname',
                'email',
                'birthdate',
            ],
        ],
        'links',
        'meta',
    ]);
    expect($response->json('meta.total'))->toBe(10);
});

it('creates a new member', function () {
    $memberData = Member::factory()->make()->toArray();

    $response = $this->postJson(route('members.store'), $memberData);

    $response->assertCreated();
    $this->assertDatabaseHas('members', $memberData);
});

it('fails to create a member with missing required fields', function () {
    $data = [
        'surname' => 'Doe',
        'email' => 'john@example.com',
        'birthdate' => '1990-01-01',
    ];

    $response = $this->postJson(route('members.store'), $data);

    $response->assertJsonFragment([
        'success' => false,
        'message' => 'Validation errors',
    ]);
    $response->assertJsonStructure([
        'success',
        'message',
        'data' => [
            'name',
        ],
    ]);
});

it('displays a specific member', function () {
    $member = Member::factory()->create();

    $response = $this->getJson(route('members.show', $member));

    $response->assertOk();
    $response->assertJson([
        'data' => [
            'id' => $member->id,
            'name' => $member->name,
            'surname' => $member->surname,
            'email' => $member->email,
            'birthdate' => $member->birthdate->format('Y-m-d'),
        ],
    ]);
});

it('displays a specific member with tags', function () {
    $member = Member::factory()
        ->has(MemberTag::factory()->count(5))
        ->create();

    $response = $this->getJson(route('members.show', ['member' => $member->id]).'?tags=1');

    $response->assertOk();
    $response->assertJsonStructure([
        'data' => [
            'member_tags' => [
                '*' => [
                    'id',
                    'name',
                ],
            ],
        ],
    ]);
});

it('updates a member', function () {
    $member = Member::factory()->create();
    $updateData = [
        'name' => 'Will',
        'surname' => 'Smith',
        'email' => 'will@smith.com',
        'birthdate' => '1968-09-25',
    ];

    $response = $this->putJson(route('members.update', $member), $updateData);

    $response->assertOk();
    $this->assertDatabaseHas('members', $updateData);
});

it('deletes a member', function () {
    $member = Member::factory()->create();

    $response = $this->deleteJson(route('members.destroy', $member));

    $response->assertNoContent();
});

it('can add a tag to a member', function () {
    $member = Member::factory()->create();
    $tag = MemberTag::factory()->create();

    $tagData = [
        'member_tag_id' => $tag->id,
    ];

    $response = $this->postJson(route('members.tags', $member), $tagData);

    $response->assertOk();
    $this->assertDatabaseHas('member_member_tag', [
        'member_id' => $member->id,
        'member_tag_id' => $tag->id,
    ]);
});
