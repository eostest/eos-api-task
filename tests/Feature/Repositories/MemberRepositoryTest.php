<?php

use App\Models\Member;
use App\Models\MemberTag;
use App\Repositories\MemberRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

it('retrieves all members', function () {
    Member::factory()->count(3)->create();

    $repository = new MemberRepository();
    $members = $repository->getAllMembers();

    expect($members)
        ->toBeInstanceOf(Collection::class)
        ->toHaveCount(3);
});

it('retrieves paginated members', function () {
    Member::factory()->count(15)->create();

    $itemsPerPage = 5;
    $repository = new MemberRepository();
    $paginatedMembers = $repository->getPaginatedMembers($itemsPerPage);

    expect($paginatedMembers)
        ->toBeInstanceOf(LengthAwarePaginator::class)
        ->and($paginatedMembers->count())->toBe($itemsPerPage);
});

it('retrieves a specific member by id', function () {
    $member = Member::factory()->create();

    $repository = new MemberRepository();
    $foundMember = $repository->getMemberById($member);

    expect($foundMember)
        ->toBeInstanceOf(Member::class)
        ->and($foundMember->id)->toBe($member->id);
});

it('creates a new member', function () {
    $data = [
        'name' => 'Will',
        'surname' => 'Smith',
        'email' => 'will@smith.com',
        'birthdate' => '1968-09-25',
    ];

    $repository = new MemberRepository();
    $newMember = $repository->createMember($data);

    expect($newMember)
        ->toBeInstanceOf(Member::class)
        ->and($newMember->name)->toEqual($data['name'])
        ->and($newMember->surname)->toEqual($data['surname'])
        ->and($newMember->email)->toEqual($data['email'])
        ->and($newMember->birthdate->format('Y-m-d'))->toEqual($data['birthdate']);
});

it('updates a member', function () {
    $member = Member::factory()->create();
    $updateData = [
        'name' => 'Will',
        'surname' => 'Smith',
        'email' => 'will@smith.com',
        'birthdate' => '1968-09-25',
    ];

    $repository = new MemberRepository();
    $updatedMember = $repository->updateMember($member, $updateData);

    expect($updatedMember)
        ->toBeInstanceOf(Member::class)
        ->and($updatedMember->name)->toEqual('Will')
        ->and($updatedMember->surname)->toEqual('Smith')
        ->and($updatedMember->email)->toEqual('will@smith.com')
        ->and($updatedMember->birthdate)->format('Y-m-d')->toEqual('1968-09-25');
});

it('deletes a member', function () {
    $member = Member::factory()->create();

    $repository = new MemberRepository();
    $repository->deleteMember($member);
    $foundMember = Member::find($member->id);

    expect($foundMember)->toBeNull();
});

it('adds a tag to a member', function () {
    $member = Member::factory()->create();
    $tag = MemberTag::factory()->create();

    $repository = new MemberRepository();
    $repository->addTagToMember($member, ['member_tag_id' => $tag->id]);

    $this->assertDatabaseHas('member_member_tag', [
        'member_id' => $member->id,
        'member_tag_id' => $tag->id,
    ]);
});
